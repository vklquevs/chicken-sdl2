# chicken-sdl2

[CHICKEN Scheme](http://call-cc.org/) bindings to
[Simple DirectMedia Layer](http://libsdl.org/) 2 (SDL2).

- Version:     0.2.1 (in development)
- Project:     https://gitlab.com/chicken-sdl2/chicken-sdl2
- Issues:      https://gitlab.com/chicken-sdl2/chicken-sdl2/issues
- API Docs:    http://api.call-cc.org/doc/sdl2
- License:     [BSD 2-Clause](LICENSE-BSD.txt)
- Maintainer:  John Croisant (john+chicken at croisant dot net)


## Synopsis

chicken-sdl2 (aka "the sdl2 egg") makes it easy to use
Simple DirectMedia Layer version 2 (SDL2) from the CHICKEN Scheme language.
SDL2 is a cross-platform library for making games and other multi-media software.

chicken-sdl2 provides a programmer-friendly, convenient, and
CHICKEN-idiomatic interface to SDL2. It takes care of the annoying
low-level C stuff for you, so you can focus on making your game.

If a feature you need is not yet available in chicken-sdl2, please
[file a feature request](CONTRIBUTING.md#filing-feature-requests) or
contact a maintainer, so we can prioritize adding it.


## Installation

If you run into trouble installing chicken-sdl2, please
[file a support request](CONTRIBUTING.md#filing-support-requests) so
we can help you, and so we can improve the install process and
instructions for future users.

### Dependencies

chicken-sdl2 requires **SDL version 2.0.0 or higher**. It will not
work with SDL 1. (For SDL 1, try the
[sdl-base](http://wiki.call-cc.org/eggref/4/sdl-base) egg instead.)

chicken-sdl2 requires CHICKEN Scheme 4.8 or higher. Please file an
issue or contact the maintainer if you need to use this library with
an earlier version of CHICKEN Scheme.

### Installing the egg

Usually, you can install the sdl2 egg by running this command:

```
chicken-install sdl2
```

The installer will try to use the `sdl2-config` script
(which is distributed with SDL2)
to automatically determine the SDL2 compiler and linker flags.
If `sdl2-config` is not available on your system,
the installer will abort with a message:
`Error: Could not automatically determine SDL2 flags.`
If this happens to you, set the `SDL2_CFLAGS` and `SDL2_LIBS`
environment variables to specify the compiler and linker flags, then try again.
For example:

```
export SDL2_CFLAGS="-I/usr/local/include/SDL2"
export SDL2_LIBS="-L/usr/local/lib -lSDL2"
chicken-install sdl2
```

These environment variables are needed only when installing the egg itself.
They are not needed when compiling or running games that use the egg.

### Installing on Mac with frameworks

If you are installing on a Mac computer,
and you want to compile using the SDL2 framework,
pass the `-D sdl2-use-mac-framework` flag to `chicken-install`:

```
chicken-install sdl2 -D sdl2-use-mac-framework
```

This assumes that the framework is installed in the `/Library/Frameworks` directory.
If the framework is installed somewhere else,
you will also need to set the `SDL2_CFLAGS` and `SDL2_LIBS` flags
to specify where to find the framework:

```
export SDL2_CFLAGS="-F/path/to/your/frameworks"
export SDL2_LIBS="-F/path/to/your/frameworks -framework SDL2"
chicken-install sdl2 -D sdl2-use-mac-framework
```

### Installing from git

If you want to try out the cutting edge, not-yet-released version of this library,
you can clone the git repository and install, like so:

```
git clone https://gitlab.com/chicken-sdl2/chicken-sdl2.git
cd chicken-sdl2
chicken-install
```

(Notice that there is no `sdl2` argument after `chicken-install`.)

You may need to set the `SDL2_CFLAGS` and `SDL2_LIBS` environment variables,
as described above.


## Demos and Examples

After you have installed chicken-sdl2, you can try compiling and
running chicken-sdl2 demos and examples.

The [demos directory](https://gitlab.com/chicken-sdl2/chicken-sdl2/tree/master/demos)
contains small programs demonstrating how to use various features of chicken-sdl2.
For example, to compile and run "The Sea and The Stars" demo:

```
csc -O3 ./demos/sea-and-stars.scm
./demos/sea-and-stars
```

The [chicken-sdl2-examples repository](https://gitlab.com/chicken-sdl2/chicken-sdl2-examples)
contains complete example games and programs made with chicken-sdl2 and related libraries.


## Related libraries

- [chicken-sdl2-image](https://gitlab.com/chicken-sdl2/chicken-sdl2-image):
  Bindings to [SDL_image](http://www.libsdl.org/projects/SDL_image/) 2
  (image file loading)

- [chicken-sdl2-ttf](https://gitlab.com/chicken-sdl2/chicken-sdl2-ttf):
  Bindings to [SDL_ttf](http://www.libsdl.org/projects/SDL_ttf/) 2
  (text rendering using TTF fonts)


## Contributing

chicken-sdl2 is a volunteer effort, and your help is appreciated.
There are many ways to get involved in the project, whether you are an
experienced programmer or not. For more information about how you can
help, please see the [contribution guide](CONTRIBUTING.md).

Please be aware that all project participants are expected to abide by
the [Contributor Code of Conduct](CODE_OF_CONDUCT.md). We are
committed to making participation in this project a welcoming and
harassment-free experience.
